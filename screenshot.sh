# This file is part of simple-screenshot-handler-script.
#
# simple-screenshot-handler-script is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# simple-screenshot-handler-script is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with simple-screenshot-handler-script.
# If not, see <https://www.gnu.org/licenses/>. 
#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

# where the image is temporarily saved.
# image is saved temporarily to use it as the notification icon.
tmp_image=/tmp/screenshot_script.png

print_help()
{
    echo "Usage: screenshot.sh <select mode> <delay_interact> <delay_info>"
    echo ""
    echo "Take screenshots and get a notification to do more with it."
    echo ""
    echo "Options:"
    echo "    -h - show this help"
    echo "    <select mode>"
    echo "        s - to select an area to capture"
    echo "        w - to capture currently focused window"
    echo "        f - for fullscreen capture"
    echo "    Optional:"
    echo "        <delay_interact> - amount of time (in ms) interactable notifications stay on screen (default: 5000)"
    echo "        <delay_info> - amount of time (in ms) informative notifications stay on screen (default: 3000)"
    echo ""
    echo "simple-screenshot-handler-script - Screenshot script by Okxa"
}
# if no args
if [ $# -eq 0 ]; then
    print_help
    exit
fi
# set delay defaults if not supplied
if [ -n "$2" ]; then
    delay_interact=$2
else
    delay_interact=5000
fi
if [ -n "$3" ]; then
    delay_info=$3
else
    delay_info=3000
fi
#if 1st arg
if [ "$1" = "s" ]; then
    scrot -opfs $tmp_image
elif [ "$1" = "w" ]; then
    scrot -opfu $tmp_image
elif [ "$1" = "f" ]; then
    scrot -opf $tmp_image
elif [ "$1" = "-h" ]; then
    print_help
    exit
else
    print_help
    exit
fi

# if scrot returns 0, we got a screenshot
if [ "${PIPESTATUS[0]}" = "0" ]; then
    xclip -selection clipboard -t image/png $tmp_image
    $SCRIPTPATH/screenshot-notify.sh $tmp_image $delay_interact $delay_info # call the notification script
fi
