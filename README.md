# Simple Screenshot Handler Script

A set of shell scripts to handle taking screenshots on linux, using small programs.

![The screenshot notification as seen on XFCE notification plugin](notification.png)  
As seen on XFCE notify plugin

## What it does

Usage:

```
Usage: screenshot.sh <select mode> <delay_interact> <delay_info>

Take screenshots and get a notification to do more with it.

Options:
    -h - show this help
    <select mode>
        s - to select an area to capture
        w - to capture currently focused window
        f - for fullscreen capture
    Optional:
        <delay_interact> - amount of time (in ms) interactable notifications stay on screen (default: 5000)
        <delay_info> - amount of time (in ms) informative notifications stay on screen (default: 3000)
```

When then calling the script, it will:

1. Allow you to capture a screenshot by area or fullscreen to clipboard
2. Notification with actions shows up to select if you want to do anything else with the image than have it on the clipboard.

Available Actions:

 - Save image to local file (and prompts you afterwards if you want to open the image/location)
 - Read a barcode from the captured image.

## Requirements

Probably requires Xorg, because using Xorg specific screenshot/clipboard utilities. Perhaps a subsitutes can be found for wayland?

- scrot - screenshot capture
- xclip - clipboard handling
- notify-send.py - for notifications with actions.
- zenity - for save dialog
- zbarimg - for barcode scan