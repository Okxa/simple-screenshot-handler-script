# This file is part of simple-screenshot-handler-script.
#
# simple-screenshot-handler-script is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# simple-screenshot-handler-script is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with simple-screenshot-handler-script.
# If not, see <https://www.gnu.org/licenses/>. 
#!/bin/bash

# this script uses a image from clipboard
# it should be called only after we have a image on clipboard.
# preferably from the screenshot.sh supplied

# arguments
# <temp image path> <initial notification delay in ms> <after action notification delay in ms>
# temp image path is used for notification icon

# set delay defaults if not supplied
if [ -n "$1" ]; then
    delay_interact=$2
else
    delay_interact=5000
fi
if [ -n "$2" ]; then
    delay_info=$3
else
    delay_info=3000
fi
# notify about the screenshot taken
output="$(notify-send -i $1 -t ${delay_interact} -a 'Screenshot Script' --action=save='Save to File' --action=barcode='Read barcode' 'Screenshot Captured' 'Screenshot captured to clipboard')"
# split the output string to array
outputarr=($output)
# get only the action
act=${outputarr[0]}

# if action 
if [ "$act" == "save" ]; then
    # get save path w/ zenity
    savepath=$(zenity --file-selection --save --filename="$HOME/Screenshots/screenshot.png")
    # check if actually have a path, cancel does not give a path
    if [ -n "${savepath}" ]; then
        xclip -selection clipboard -o > $savepath
        saveoutput=$(notify-send -i stock_save -t $delay_interact -a 'Screenshot Script' 'Image saved' "Saved image to:\n$savepath" --action=open='Open Image' --action=openfolder='Open location')
        saveactarr=($saveoutput)
        saveact=${saveactarr[0]}
        if [ "$saveact" == "open" ]; then
            xdg-open $savepath
        elif [ "$saveact" == "openfolder" ]; then
            imgdir=${savepath%/*} # get folder path
            xdg-open $imgdir
        fi
    else
        notify-send -i stock_save -t 3000 -a 'Screenshot Script' 'Image not saved'
    fi
# if clicked button was barcode, read it
elif [ "$act" == "barcode" ]; then
    code=$(xclip -selection clipboard -o | zbarimg -q --raw -)
    # test if not empty return
    if [ -n "${code}" ]; then
        # send to clipboard if read anything
        echo $code | xclip -selection clipboard
		notify-send -i stock_copy -t $delay_info -a 'Screenshot Script' 'Barcode read successfully' "Copied barcode value to clipboard.\nCode: $code"
    else
		notify-send -i stock_copy -t $delay_info -a 'Screenshot Script' 'Barcode read failed.' "Image does not contain a barcode."
    fi
fi
